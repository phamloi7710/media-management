<div class="media-modal mm-animated fadeIn is-active media-modal-manager__Inmodal">
    <div class="media-modal-background" @click.stop="hideInputModal()"></div>
    <div class="media-modal-content mm-animated fadeInDown">
        <div class="box">
            @include('media::_manager', [
                'modal' => true,
                'restrict' => [
                    'uploadTypes' => [
                        '*'
                    ]
                ],
                'hideExt' => ['svg'],
                'uploadSize' => 5000000
            ])
        </div>
    </div>
    <button class="media-modal-close is-large is-hidden-touch" @click.stop="hideInputModal()"></button>
</div>
