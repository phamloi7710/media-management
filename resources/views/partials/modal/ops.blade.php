<section>
    {{-- preview_modal --}}
    <div v-if="isActiveModal('preview_modal')"
        class="media-modal mm-animated fadeIn is-active __media-modal-preview">
        <div class="media-modal-background link" @click.stop="toggleModal()"></div>
        <div class="mm-animated fadeInDown __media-modal-content-wrapper">
            <transition :name="`mm-img-${imageSlideDirection}`"
                mode="out-in"
                appear>
                <div class="media-modal-content" :key="selectedFile.path">
                    {{-- card v --}}
                    @include('media::partials.card')
                </div>
            </transition>
        </div>
        <button class="media-modal-close is-large" @click.stop="toggleModal()"></button>
    </div>

    {{-- image_editor --}}
    <div v-if="isActiveModal('imageEditor_modal')"
        class="media-modal mm-animated fadeIn is-active __media-modal-editor">
        <v-touch class="media-modal-background link" @dbltap="toggleModal()"></v-touch>
        <div class="mm-animated fadeInDown __media-modal-content-wrapper">
            <image-editor route="{{ route('media.uploadCropped') }}"
                :no-scroll="noScroll"
                :file="selectedFile"
                :trans="trans">
            </image-editor>
        </div>
        <button class="media-modal-close is-large" @click.stop="toggleModal()"></button>
    </div>

    {{-- save_link --}}
    <div class="media-modal mm-animated fadeIn"
        :class="{'is-active': isActiveModal('save_link_modal')}">
        <div class="media-modal-background link" @click.stop="toggleModal()"></div>
        <div class="media-modal-card mm-animated fadeInDown">
            <header class="media-modal-card-head is-black">
                <p class="media-modal-card-title">
                    <span>{{ trans('media::messages.save.link') }}</span>
                </p>
                <button type="button" class="delete" @click.stop="toggleModal()"></button>
            </header>

            <form action="{{ route('media.uploadLink') }}" @submit.prevent="saveLinkForm($event)">
                <section class="media-modal-card-body">
                    <input class="input" type="text"
                        v-model="urlToUpload"
                        placeholder="{{ trans('media::messages.add.url') }}"
                        ref="save_link_modal_input">
                </section>
                <footer class="media-modal-card-foot">
                    <button type="reset" class="button" @click.stop="toggleModal()">
                        {{ trans('media::messages.cancel') }}
                    </button>
                    <button type="submit"
                        class="button is-link"
                        :disabled="isLoading"
                        :class="{'is-loading': isLoading}">
                        {{ trans('media::messages.upload.main') }}
                    </button>
                </footer>
            </form>
        </div>
    </div>

    {{-- new_folder_modal --}}
    <div class="media-modal mm-animated fadeIn"
        :class="{'is-active': isActiveModal('new_folder_modal')}">
        <div class="media-modal-background link" @click.stop="toggleModal()"></div>
        <div class="media-modal-card mm-animated fadeInDown">
            <header class="media-modal-card-head is-link">
                <p class="media-modal-card-title">
                    <span>{{ trans('media::messages.add.new_folder') }}</span>
                </p>
                <button type="button" class="delete" @click.stop="toggleModal()"></button>
            </header>

            <form action="{{ route('media.new_folder') }}" @submit.prevent="NewFolderForm($event)">
                <section class="media-modal-card-body">
                    <input class="input" type="text"
                        v-model="newFolderName"
                        placeholder="{{ trans('media::messages.new.folder_name') }}"
                        ref="new_folder_modal_input">
                </section>
                <footer class="media-modal-card-foot">
                    <button type="reset" class="button" @click.stop="toggleModal()">
                        {{ trans('media::messages.cancel') }}
                    </button>
                    <button type="submit"
                        class="button is-link"
                        :disabled="isLoading"
                        :class="{'is-loading': isLoading}">
                        {{ trans('media::messages.new.create_folder') }}
                    </button>
                </footer>
            </form>
        </div>
    </div>

    {{-- rename_file_modal --}}
    <div class="media-modal mm-animated fadeIn"
        :class="{'is-active': isActiveModal('rename_file_modal')}">
        <div class="media-modal-background link" @click.stop="toggleModal()"></div>
        <div class="media-modal-card mm-animated fadeInDown">
            <header class="media-modal-card-head is-warning">
                <p class="media-modal-card-title">
                    <span>{{ trans('media::messages.rename.file_folder') }}</span>
                </p>
                <button type="button" class="delete" @click.stop="toggleModal()"></button>
            </header>

            <form action="{{ route('media.rename_file') }}" @submit.prevent="RenameFileForm($event)">
                <section class="media-modal-card-body">
                    <h3 class="title">{{ trans('media::messages.new.file_folder') }}</h3>
                    <input class="input" type="text"
                        v-if="selectedFile"
                        v-model="newFilename"
                        ref="rename_file_modal_input"
                        @focus="newFilename = selectedFileIs('folder') ? selectedFile.name : getFileName(selectedFile.name)">
                </section>
                <footer class="media-modal-card-foot">
                    <button type="reset" class="button" @click.stop="toggleModal()">
                        {{ trans('media::messages.cancel') }}
                    </button>
                    <button type="submit"
                        class="button is-warning"
                        :disabled="isLoading"
                        :class="{'is-loading': isLoading}">
                        {{ trans('media::messages.rename.main') }}
                    </button>
                </footer>
            </form>
        </div>
    </div>

    {{-- move_file_modal --}}
    <div class="media-modal mm-animated fadeIn"
        :class="{'is-active': isActiveModal('move_file_modal')}">
        <div class="media-modal-background link" @click.stop="toggleModal()"></div>
        <form class="media-modal-card mm-animated fadeInDown"
            action="{{ route('media.move_file') }}"
            @submit.prevent="MoveFileForm($event)">
            <header class="media-modal-card-head is-warning">
                <p class="media-modal-card-title">
                    <transition :name="copyFilesNotMove ? 'mm-info-in' : 'mm-info-out'" mode="out-in">
                        <span class="icon" :key="copyFilesNotMove ? 1 : 2">
                            <icon :name="copyFilesNotMove ? 'regular/clone' : 'share'"></icon>
                        </span>
                    </transition>

                    <transition name="mm-list" mode="out-in">
                        <span key="1" v-if="copyFilesNotMove">{{ trans('media::messages.copy.file_folder') }}</span>
                        <span key="2" v-else>{{ trans('media::messages.move.file_folder') }}</span>
                    </transition>
                </p>
                <button type="button" class="delete" @click.stop="toggleModal()"></button>
            </header>

            <section class="media-modal-card-body">
                {{-- destination --}}
                <h5 class="subtitle m-b-20">
                    {{ trans('media::messages.destination_folder') }} :
                    <span class="tag is-black is-medium">@{{ files.path || '/' }}</span>
                </h5>

                @include('media::partials.modal.mov-files-info')
            </section>

            <footer class="modal-card-foot">
                <div class="level is-mobile full-width">
                    {{-- switcher --}}
                    <div class="level-left">
                        <div class="level-item">
                            <div class="form-switcher">
                                <input type="checkbox" name="use_copy" id="use_copy" v-model="copyFilesNotMove">
                                <label class="switcher" for="use_copy"></label>
                            </div>
                        </div>
                        <div class="level-item">
                            <label class="label" for="use_copy">{{ trans('media::messages.copy.files') }}</label>
                        </div>
                    </div>

                    {{-- btns --}}
                    <div class="level-right">
                        <div class="level-item">
                            <button type="reset"
                                class="button"
                                @click.stop="toggleModal()">
                                {{ trans('media::messages.cancel') }}
                            </button>
                        </div>
                        <div class="level-item">
                            <button type="reset"
                                class="button is-danger"
                                @click.stop="clearMovableList()">
                                {{ trans('media::messages.crop.reset') }}
                            </button>
                        </div>
                        <div class="level-item">
                            <button type="submit"
                                ref="move_file_modal_submit"
                                class="button is-warning"
                                :disabled="isLoading"
                                :class="{'is-loading': isLoading}">
                                <span>{{ trans('media::messages.paste') }}</span>
                            </button>
                        </div>
                    </div>
                </div>
            </footer>
        </form>
    </div>

    {{-- confirm_delete_modal --}}
    <div class="media-modal mm-animated fadeIn"
        :class="{'is-active': isActiveModal('confirm_delete_modal')}">
        <div class="media-modal-background link" @click.stop="toggleModal()"></div>
        <div class="media-modal-card mm-animated fadeInDown">
            <header class="media-modal-card-head is-danger">
                <p class="media-modal-card-title">
                    <span>{{ trans('media::messages.are_you_sure_delete') }}</span>
                </p>
                <button type="button" class="delete" @click.stop="toggleModal()"></button>
            </header>

            <form action="{{ route('media.delete_file') }}"
                @submit.prevent="DeleteFileForm($event)">
                <section class="media-modal-card-body">
                    @include('media::partials.modal.del-files-info')

                    {{-- deleting folder warning --}}
                    <h5 v-show="folderDeleteWarning" class="__media-modal-folder-warning">
                        <span class="icon is-medium"><icon name="exclamation-triangle" scale="1.2"></icon></span>
                        <span>{{ trans('media::messages.delete.folder') }}</span>
                    </h5>
                </section>

                <footer class="media-modal-card-foot">
                    <button type="reset" class="button" @click.stop="toggleModal()">
                        {{ trans('media::messages.cancel') }}
                    </button>
                    <button type="submit"
                        ref="confirm_delete_modal_submit"
                        class="button is-danger"
                        :disabled="isLoading"
                        :class="{'is-loading': isLoading}">
                        {{ trans('media::messages.delete.confirm') }}
                    </button>
                </footer>
            </form>
        </div>
    </div>

{{--    <example-comp inline-template>--}}
{{--        <div>--}}
{{--            --}}{{-- manager --}}
{{--            <div>@include('media::extras.modal')</div>--}}

{{--            --}}{{-- editor --}}
{{--            @include('media::extras.editor')--}}

{{--            --}}{{-- form --}}
{{--            // your form--}}
{{--        </div>--}}
{{--    </example-comp>--}}

{{--    <div class="media-modal mm-animated fadeIn"--}}
{{--         :class="{'is-active': isActiveModal('select_files_modal')}">--}}
{{--        <div class="media-modal-background link" @click.stop="toggleModal()"></div>--}}
{{--        <div class="media-modal-card mm-animated fadeInDown">--}}
{{--            <header class="media-modal-card-head is-link">--}}
{{--                <p class="media-modal-card-title">--}}
{{--                    <span>{{ trans('media::messages.add.new_folder') }}</span>--}}
{{--                </p>--}}
{{--                <button type="button" class="delete" @click.stop="toggleModal()"></button>--}}
{{--            </header>--}}

{{--            <form action="{{ route('media.new_folder') }}" @submit.prevent="NewFolderForm($event)">--}}
{{--                <section class="media-modal-card-body">--}}
{{--                    <input class="input" type="text"--}}
{{--                           v-model="newFolderName"--}}
{{--                           placeholder="{{ trans('media::messages.new.folder_name') }}"--}}
{{--                           ref="select_files_modal_input">--}}
{{--                </section>--}}
{{--                <footer class="media-modal-card-foot">--}}
{{--                    <button type="reset" class="button" @click.stop="toggleModal()">--}}
{{--                        {{ trans('media::messages.cancel') }}--}}
{{--                    </button>--}}
{{--                    <button type="submit"--}}
{{--                            class="button is-link"--}}
{{--                            :disabled="isLoading"--}}
{{--                            :class="{'is-loading': isLoading}">--}}
{{--                        {{ trans('media::messages.new.create_folder') }}--}}
{{--                    </button>--}}
{{--                </footer>--}}
{{--            </form>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--    <media-modal :restrict="{--}}
{{--   'path': 'users/admin/pdfs',--}}
{{--   'uploadTypes': ['image/*', 'application/pdf'],--}}
{{--   'uploadSize': 5--}}
{{--}" etc..></media-modal>--}}
{{--    <files-select inline-template>--}}
{{--        <div>--}}
{{--            --}}{{-- manager --}}
{{--            <div v-if="inputName">@include('media::extras.modal')</div>--}}

{{--            --}}{{-- items selector --}}
{{--            <media-modal item="cover" :name="inputName"></media-modal>--}}
{{--            <media-modal item="gallery" :name="inputName" type="folder"></media-modal>--}}
{{--            <media-modal item="links" :name="inputName" :multi="true"></media-modal>--}}

{{--            --}}{{-- for editor --}}
{{--            @include('media::extras.editor')--}}

{{--            --}}{{-- form --}}
{{--            <form action="..." method="...">--}}
{{--                --}}{{-- cover --}}
{{--                <section>--}}
{{--                    <img :src="cover">--}}
{{--                    <input type="hidden" name="cover" :value="cover"/>--}}
{{--                    <button @click="toggleModalFor('cover')">select cover</button>--}}
{{--                </section>--}}

{{--                --}}{{-- gallery --}}
{{--                <section>--}}
{{--                    <input type="text" name="gallery" :value="gallery"/>--}}
{{--                    <button @click="toggleModalFor('gallery')">select gallery folder</button>--}}
{{--                </section>--}}

{{--                --}}{{-- links --}}
{{--                <section>--}}
{{--                    <input v-for="item in links"--}}
{{--                           :key="item"--}}
{{--                           :value="item"--}}
{{--                           type="text"--}}
{{--                           name="links[]"/>--}}

{{--                    <button @click="toggleModalFor('links')">select gallery links</button>--}}
{{--                </section>--}}

{{--                // ...--}}
{{--            </form>--}}
{{--        </div>--}}
{{--    </files-select>--}}
</section>
