<div class="image-preview media--@php echo $element_name; @endphp">
    <div class="image">
        @if(isset($multi))
            @if(!isset($value))
                <div v-if="links.length === 0">
                    <img src="{!! url('app-assets/media/no-image.png') !!}">
                </div>
                <div class="multiple-images" v-if="links.length > 0">
                    <div class="image-item" v-for="item in links">
                        <div>
                            <img :src="item">
                            <input
                                    :key="item"
                                    :value="item"
                                    type="hidden"
                                    name="{!! $element_name !!}[]"/>
                        </div>
                        <button v-if="links" type="button" class="button is-danger remove-image" @click="removeImageFromList(item)" title="@lang('media::messages.remove_image')">
                            <span>✘</span>
                        </button>
                    </div>
                </div>
            @else
                <div class="multiple-images">
                    @foreach($value as $item)
                        <div class="image-item">
                            <div>
                                <img src="{!! url($item ?: '') !!}">
                            </div>
                            <button type="button"
                                    class="button is-danger remove-image"
                                    title="@lang('media::messages.remove_image')">
                                <span>✘</span>
                            </button>
                            <input
                                value="{!! $item !!}"
                                type="hidden"
                                name="{!! $element_name !!}[]"/>
                        </div>
                    @endforeach

                    <div class="image-item" v-for="item in links">
                        <div>
                            <img :src="item">
                        </div>
                        <button type="button" class="button is-danger remove-image" @click="removeImageFromList(item)" title="@lang('media::messages.remove_image')">
                            <span>✘</span>
                        </button>
                        <input
                               :key="item"
                               :value="item"
                               type="hidden"
                               name="{!! $element_name !!}[]"/>
                    </div>
                </div>
            @endif
        @else
            <div class="img">
                <img v-if="!imageSelected" src="{!! url('app-assets/media/no-image.png') !!}">
                <img v-if="imageSelected" :src="imageSelected" class="{!! $element_name !!}">
            </div>
            <input v-if="!imageSelected" type="hidden" class="{!! $element_name !!}" name="{!! $element_name !!}" :value="'app-assets/media/no-image.png'">
            <input v-if="imageSelected" type="hidden" class="{!! $element_name !!}" name="{!! $element_name !!}" :value="imageSelected">
            <button v-if="imageSelected" type="button" class="button is-danger remove-image" @click="removeImage()" title="@lang('media::messages.remove_image')">
                <span>✘</span>
            </button>
        @endif
    </div>
    <div class="actions">
        @if(isset($multi))
            <button type="button" @click="toggleModalFor('links')" class="btn btn-mat btn-inverse select-image">@lang('media::messages.select.select_file')</button>
        @else
            <button type="button" @click="toggleModalFor('imageSelected')" class="btn btn-mat btn-inverse select-image">@lang('media::messages.select.select_file')</button>
        @endif
    </div>
    @if($errors->has($element_name))
        <div class="messages">
            <span class="text-danger error">{!! $errors->first($element_name) !!}</span>
        </div>
    @endif
</div>

