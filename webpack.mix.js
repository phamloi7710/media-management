const mix = require('laravel-mix')
const webpack = require('webpack')
// BABEL config
mix.webpackConfig({
    plugins: [
        new webpack.ProvidePlugin({
            process: 'process/browser',
            Buffer: ['buffer', 'Buffer']
        })
    ]
})
// MediaManager
mix.copyDirectory('resources/app-assets/js/packages', 'public/app-assets/media/packages');
mix.copyDirectory('resources/app-assets/dist', 'public/app-assets/media');
mix.js('resources/app-assets/js/media-manager.js', 'public/app-assets/media/js/media-manager.min.js').vue();
