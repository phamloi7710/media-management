<?php

    if(!function_exists("media_path")) {
        /**
         * @return string directory
         */
        function media_path(){
            $path = base_path("packages/media-management/");

            if(is_dir($path)) {
                return $path;
            } else {
                return base_path("vendor/loipham/media-management/");
            }
        }
    }
