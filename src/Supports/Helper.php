<?php

    namespace LoiPham\Media\Supports;

    use Illuminate\Support\Facades\Artisan;
    use Illuminate\Support\Facades\Auth;
    use Illuminate\Database\Eloquent\Model as Eloquent;
    use Exception;
    use Illuminate\Support\Facades\File;
    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Support\Facades\Request;

    class Helper
    {
        /**
         * Load module's helpers
         * @param $directory
         *
         * @since 2.0
         */
        public static function autoload($directory)
        {
            $helpers = File::glob($directory . '/*.php');
            foreach ($helpers as $helper) {
                File::requireOnce($helper);
            }
        }
    }
