<?php

namespace LoiPham\Media\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use LoiPham\Media\Controllers\Modules\GlobalSearch;
use LoiPham\Media\Controllers\Modules\Lock;
use LoiPham\Media\Controllers\Modules\Move;
use LoiPham\Media\Controllers\Modules\Utils;
use LoiPham\Media\Controllers\Modules\Delete;
use LoiPham\Media\Controllers\Modules\Rename;
use LoiPham\Media\Controllers\Modules\Upload;
use LoiPham\Media\Controllers\Modules\Download;
use LoiPham\Media\Controllers\Modules\NewFolder;
use LoiPham\Media\Controllers\Modules\GetContent;
use LoiPham\Media\Controllers\Modules\Visibility;

class MediaController extends Controller
{
    use Utils,
        GetContent,
        Delete,
        Download,
        Lock,
        Move,
        Rename,
        Upload,
        NewFolder,
        Visibility,
        GlobalSearch;

    protected $baseUrl;
    protected $db;
    protected $file_table;
    protected $fileChars;
    protected $fileSystem;
    protected $folderChars;
    protected $ignoreFiles;
    protected $LMF;
    protected $GFI;
    protected $sanitizedText;
    protected $storageDisk;
    protected $storageDiskInfo;
    protected $unallowedMimes;
    protected $unallowedExt;

    public function __construct()
    {
        $this->fileSystem     = config('media.storage_disk');
        $this->ignoreFiles    = config('media.ignore_files');
        $this->fileChars      = config('media.allowed_fileNames_chars');
        $this->folderChars    = config('media.allowed_folderNames_chars');
        $this->sanitizedText  = config('media.sanitized_text');
        $this->unallowedMimes = config('media.unallowed_mimes');
        $this->LMF            = config('media.last_modified_format');
        $this->GFI            = config('media.get_folder_info') ?? true;

        $this->storageDisk     = app('filesystem')->disk($this->fileSystem);
        $this->storageDiskInfo = app('config')->get("filesystems.disks.{$this->fileSystem}");
        $this->baseUrl         = $this->storageDisk->url('/');
        //$this->db              = app('db')->connection('mysql')->table('media_manager_file_locked');
        //$this->file_table      = app('db')->connection('mysql')->table('media_manager_files');
        $this->file_table      = DB::table(config('media.media_table'));

    }

    /**
     * @param      $input
     * @param bool $loseCase
     * @return false|string|string[]
     */
    public function seoVietnamese($input, $loseCase = true)
    {

        $input = strip_tags($input);


        $markVietNamese = [
            "à",
            "á",
            "ạ",
            "ả",
            "ã",
            "â",
            "ầ",
            "ấ",
            "ậ",
            "ẩ",
            "ẫ",
            "ă",
            "ằ",
            "ắ",
            "ặ",
            "ẳ",
            "ẵ",
            "è",
            "é",
            "ẹ",
            "ẻ",
            "ẽ",
            "ê",
            "ề",
            "ế",
            "ệ",
            "ể",
            "ễ",
            "ì",
            "í",
            "ị",
            "ỉ",
            "ĩ",
            "ò",
            "ó",
            "ọ",
            "ỏ",
            "õ",
            "ô",
            "ồ",
            "ố",
            "ộ",
            "ổ",
            "ỗ",
            "ơ",
            "ờ",
            "ớ",
            "ợ",
            "ở",
            "ỡ",
            "ù",
            "ú",
            "ụ",
            "ủ",
            "ũ",
            "ư",
            "ừ",
            "ứ",
            "ự",
            "ử",
            "ữ",
            "ỳ",
            "ý",
            "ỵ",
            "ỷ",
            "ỹ",
            "đ",
            "À",
            "Á",
            "Ạ",
            "Ả",
            "Ã",
            "Â",
            "Ầ",
            "Ấ",
            "Ậ",
            "Ẩ",
            "Ẫ",
            "Ă",
            "Ằ",
            "Ắ",
            "Ặ",
            "Ẳ",
            "Ẵ",
            "È",
            "É",
            "Ẹ",
            "Ẻ",
            "Ẽ",
            "Ê",
            "Ề",
            "Ế",
            "Ệ",
            "Ể",
            "Ễ",
            "Ì",
            "Í",
            "Ị",
            "Ỉ",
            "Ĩ",
            "Ò",
            "Ó",
            "Ọ",
            "Ỏ",
            "Õ",
            "Ô",
            "Ồ",
            "Ố",
            "Ộ",
            "Ổ",
            "Ỗ",
            "Ơ",
            "Ờ",
            "Ớ",
            "Ợ",
            "Ở",
            "Ỡ",
            "Ù",
            "Ú",
            "Ụ",
            "Ủ",
            "Ũ",
            "Ư",
            "Ừ",
            "Ứ",
            "Ự",
            "Ử",
            "Ữ",
            "Ỳ",
            "Ý",
            "Ỵ",
            "Ỷ",
            "Ỹ",
            "Đ"
        ];

        $replaceAlphabet = [
            "a",
            "a",
            "a",
            "a",
            "a",
            "a",
            "a",
            "a",
            "a",
            "a",
            "a",
            "a",
            "a",
            "a",
            "a",
            "a",
            "a",
            "e",
            "e",
            "e",
            "e",
            "e",
            "e",
            "e",
            "e",
            "e",
            "e",
            "e",
            "i",
            "i",
            "i",
            "i",
            "i",
            "o",
            "o",
            "o",
            "o",
            "o",
            "o",
            "o",
            "o",
            "o",
            "o",
            "o",
            "o",
            "o",
            "o",
            "o",
            "o",
            "o",
            "u",
            "u",
            "u",
            "u",
            "u",
            "u",
            "u",
            "u",
            "u",
            "u",
            "u",
            "y",
            "y",
            "y",
            "y",
            "y",
            "d",
            "A",
            "A",
            "A",
            "A",
            "A",
            "A",
            "A",
            "A",
            "A",
            "A",
            "A",
            "A",
            "A",
            "A",
            "A",
            "A",
            "A",
            "E",
            "E",
            "E",
            "E",
            "E",
            "E",
            "E",
            "E",
            "E",
            "E",
            "E",
            "I",
            "I",
            "I",
            "I",
            "I",
            "O",
            "O",
            "O",
            "O",
            "O",
            "O",
            "O",
            "O",
            "O",
            "O",
            "O",
            "O",
            "O",
            "O",
            "O",
            "O",
            "O",
            "U",
            "U",
            "U",
            "U",
            "U",
            "U",
            "U",
            "U",
            "U",
            "U",
            "U",
            "Y",
            "Y",
            "Y",
            "Y",
            "Y",
            "D"
        ];
        $str = str_replace([
            "`",
            "=",
            "&",
            "#",
            "*",
            "@",
            "^",
            ";",
            " ",
            "/",
            "\\",
            ")",
            "(",
            "[",
            "]",
            "!",
            "?",
            "|",
            ",",
            '–',
            '"',
            '\'',
            '"',
            '"',
            '&quot;',
            '.',
            ':',
            "%",
            "…"
        ], "-", str_replace($markVietNamese, $replaceAlphabet, $input));

        $str = $this->justClean($str);
        $str = str_replace("--", "-", $str);
        $str = str_replace("--", "-", $str);
        $str = str_replace("--", "-", $str);
        $str = str_replace("--", "-", $str);
        $str = str_replace("--", "-", $str);
        $str = str_replace("--", "-", $str);
        $str = str_replace("--", "-", $str);
        $str = str_replace("--", "-", $str);


        if (substr($str, strlen($str) - 1) == "-") $str = substr($str, 0, strlen($str) - 1);
        if (substr($str, 0, 1) == "-") $str = substr($str, 1);
        if ($loseCase) return strtolower($str);
        else
            return $str;
    }

    /**
     * @param $string
     * @return string|string[]|null
     */
    public function justClean($string)
    {
        // Replace other special chars
        $specialCharacters = [
            '#' => '',
            '$' => '',
            '%' => '',
            '&' => '',
            '@' => '',
            '.' => '',
            '�' => '',
            '+' => '',
            '=' => '',
            '�' => '',
            '\\' => '',
            '/' => '',
        ];

        /* while (list($character, $replacement) = each($specialCharacters))
         {
             $string = str_replace($character, '-' . $replacement . '-', $string);
         }*/
        foreach ($specialCharacters as $character => $replacement) {
            $string = str_replace($character, '-' . $replacement . '-', $string);
        }

        $string = strtr($string, "������? ����������������������������������������������", "AAAAAAaaaaaaOOOOOOooooooEEEEeeeeCcIIIIiiiiUUUUuuuuyNn");

        // Remove all remaining other unknown characters
        $string = preg_replace('/[^a-zA-Z0-9\-]/', '', $string);
        $string = preg_replace('/^[\-]+/', '-', $string);
        $string = preg_replace('/[\-]+$/', '-', $string);
        $string = preg_replace('/[\-]{2,}/', '-', $string);

        return $string;
    }
}
