<?php

namespace LoiPham\Media\Controllers\Modules;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use LoiPham\Media\Events\MediaFileOpsNotifications;

trait Lock
{
    /**
     * get locked items & directories list.
     *
     * @param [type] $dirs
     */
    public function getLockList()
    {
        return response()->json($this->lockList());
    }

    /**
     * get data.
     *
     * @param [type] $path
     */
    public function lockList()
    {
        return [
            'locked' => $this->file_table->where('locked', 'Y')->pluck('path'),
        ];
    }

    /**
     * lock/unlock files/folders.
     *
     * @param Request $request [description]
     *
     * @return [type] [description]
     */
    public function lockItem(Request $request)
    {
        $lockedList = $this->file_table->where('locked', 'Y')->pluck('path')->toArray();
        //$unLockedList = $this->file_table->where('locked', 'N')->pluck('path')->toArray();

        $toRemove = [];
        $toAdd    = [];
        $result   = [];

        foreach ($request->list as $item) {
            $url  = $item['path'];
            $name = $item['name'];

            if (in_array($url, $lockedList)) {
                $toRemove[] = $url;
            } else {
                $toAdd[] = $url;
            }


            $result[] = [
                'message' => trans('media::messages.lock_success', ['attr' => $name]),
            ];
        }

        if ($toRemove) {
            DB::table(config('media.media_table'))->whereIn('path', $toRemove)->update(['locked' => 'N']);
        }
        if ($toAdd) {
            DB::table(config('media.media_table'))->whereIn('path', $toAdd)->update(['locked' => 'Y']);
        }

        // broadcast
        broadcast(new MediaFileOpsNotifications([
            'op' => 'lock',
        ]))->toOthers();

        return compact('result');
    }
}
