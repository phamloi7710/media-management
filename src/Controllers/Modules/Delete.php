<?php

namespace LoiPham\Media\Controllers\Modules;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use LoiPham\Media\Events\MediaFileOpsNotifications;

trait Delete
{
    /**
     * delete files/folders.
     *
     * @param Request $request [description]
     *
     * @return [type] [description]
     */
    public function deleteItem(Request $request)
    {
        $path        = $request->path;
        $result      = [];
        $toBroadCast = [];
        $deleteFiles = [];
        $subDir = [];

        foreach ($request->deleted_files as $one) {
            $name      = $one['name'];
            $type      = $one['type'];
            $item_path = $one['storage_path'];
            $defaults  = [
                'name' => $name,
                'path' => $item_path,
            ];

            if($type == 'folder') {
                $subDir = $this->readPath($item_path);
                $del = $this->storageDisk->deleteDirectory($item_path);
            } else {
                $del = $this->storageDisk->delete($item_path);
            }

            $deleteFiles[] = $one['path'];

            if ($del) {
                $result[]      = array_merge($defaults, ['success' => true]);
                $toBroadCast[] = $defaults;

                // fire event
                event('MMFileDeleted', [
                    'file_path' => $item_path,
                    'is_folder' => $type == 'folder',
                ]);
            } else {
                $result[] = array_merge($defaults, [
                    'success' => false,
                    'message' => trans('media::messages.error.deleting_file'),
                ]);
            }
        }

        DB::table(config('media.media_table'))->whereIn('path', array_merge($deleteFiles, $subDir))->delete();
        // broadcast
        broadcast(new MediaFileOpsNotifications([
            'op'    => 'delete',
            'items' => $toBroadCast,
            'path'  => $path,
        ]))->toOthers();

        return response()->json($result);
    }

    public function readPath($dir, &$arrDir = []): array{
        $fullPath = storage_path('app\\public\\' . $dir);

        // Lấy danh sách các tệp tin và thư mục trong thư mục hiện tại
        $files = scandir($fullPath);

        foreach ($files as $value) {
            if($value != "." && $value != "..") {
                $arrDir[] = str_replace("\\", "/", $this->resolveUrl($dir . DIRECTORY_SEPARATOR . $value));
                // Tạo đường dẫn đầy đủ tới tệp tin hoặc thư mục
                $path = realpath($fullPath . DIRECTORY_SEPARATOR . $value);
                // Kiểm tra nếu không phải là thư mục
                if (!is_dir($path)) {
                    $arrDir[] = str_replace("\\", "/", $this->resolveUrl($dir . DIRECTORY_SEPARATOR . $value));
                } else {
                    $arrDir[] = str_replace("\\", "/", $this->resolveUrl($dir . DIRECTORY_SEPARATOR . $value));
                    // Nếu là thư mục, tiếp tục đệ quy để duyệt qua các thư mục con
                    $this->readPath($dir . DIRECTORY_SEPARATOR . $value, $arrDir);
                }
            }
        }
        return array_unique($arrDir);
    }
}
