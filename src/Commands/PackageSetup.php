<?php

    namespace LoiPham\Media\Commands;

    use Illuminate\Support\Facades\File;
    use Illuminate\Support\Str;
    use Illuminate\Console\Command;

    class PackageSetup extends Command
    {
        protected $file;
        protected $signature   = 'media:install';
        protected $description = 'Setup media package';


        /**
         * Execute the console command.
         *
         * @return mixed
         */
        public function handle()
        {
            $this->info('Đang tiến hành cài đặt media...');
            shell_exec('cd '.media_path().' && npm install');
            shell_exec('cd '.media_path().' && npm run prod');

            // Delete all files and folder in node_modules folder
            shell_exec('cd '.media_path().' && rmdir /s /q node_modules');
            File::copyDirectory(media_path().'public', public_path());
            File::copyDirectory(media_path().'resources/app-assets/images', public_path('images'));
            $this->call('storage:link');
            $this->call('migrate');
            $this->call('config:cache');
            $this->call('view:cache');
            $this->call('view:clear');
            $this->call('route:clear');
            $this->call('route:cache');
            $this->info('Quá trình cài đặt media đã hoàn tất!');
        }
    }
