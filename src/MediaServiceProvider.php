<?php

namespace LoiPham\Media;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use LoiPham\Media\Commands\PackageSetup;
use Illuminate\Support\Facades\Broadcast;

class MediaServiceProvider extends ServiceProvider
{
    protected $file;

    public function boot()
    {
        $this->file = $this->app['files'];
        $this->packagePublish();
        $this->socketRoute();
        $this->load();
        $this->viewComp();
        $this->command();
        Schema::defaultStringLength(191);
    }

    /**
     * publish package assets.
     *
     * @return [type] [description]
     */
    protected function load()
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/media.php', 'media');
        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'media');
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');
        $this->loadTranslationsFrom(__DIR__ . '/../resources/lang', 'media');
        $this->loadRoutesFrom(__DIR__ . '/Routes/Routes.php');
    }


    /**
     * share data with view.
     *
     * @return [type] [description]
     */
    protected function viewComp()
    {
        $data = [];

        // base url
        $url    = $this->app['filesystem']
                    ->disk(config('media.storage_disk'))
                    ->url('/');

        $data['base_url'] = preg_replace('/\/+$/', '/', $url);

        // upload panel bg patterns
        $pattern_path = public_path('app-assets/media/patterns');

        if ($this->file->exists($pattern_path)) {
            $patterns = collect(
                $this->file->allFiles($pattern_path)
            )->map(function ($item) {
                $name = str_replace('\\', '/', $item->getPathName());

                return preg_replace('/.*\/patterns/', '/app-assets/media/patterns', $name);
            });

            $data['patterns'] = json_encode($patterns);
        }

        // share
        view()->composer('media::_manager', function ($view) use ($data) {
            $view->with($data);
        });
    }

    /**
     * package commands.
     *
     * @return [type] [description]
     */
    protected function command()
    {
        $this->commands([
            PackageSetup::class,
        ]);
    }

    /**
     * extra functionality.
     *
     * @return [type] [description]
     */
    public function register()
    {
        \LoiPham\Media\Supports\Helper::autoload(__DIR__."/../helpers");
    }

    /**
     * publish package assets.
     *
     * @return [type] [description]
     */
    protected function packagePublish(){

        // config
        $this->publishes([
            __DIR__ . '/../config' => config_path(),
        ], 'media-manager-config');

        $this->publishes([
            __DIR__ . '/../database/migrations' => database_path('migrations'),
        ], 'media-manager-migration');

        // resources
        /*$this->publishes([
            __DIR__ . '/../resources/app-assets' => resource_path('app-assets/media-manager'),
        ], 'media-manager-assets');*/

        // trans
        $this->loadTranslationsFrom(__DIR__ . '/../resources/lang', 'media');
        $this->publishes([
            __DIR__ . '/../resources/lang' => lang_path('vendor/MediaManager'),
        ], 'media-manager-translate');

        // views
        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'media');
        $this->publishes([
            __DIR__ . '/../resources/views' => resource_path('views/vendor/media-manager'),
        ], 'media-manager-view');
    }

    protected function socketRoute()
    {
        Broadcast::channel('User.{id}.media', function ($user, $id) {
            return $user->id == $id;
        });
    }
}
